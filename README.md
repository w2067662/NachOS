********************************************
    OS project1: NachOS SystemCall
    Editor: B10613019 ChengChingRu (鄭敬儒)
********************************************
HW 1_1 :Implement SYSCALL ThreadYield

Each time ThreadYield(); called by user program, exception 
handler will release the CPU for a period of time. During 
this period of time, if there is next_thread, then 
partial instructions of next_thread will be processed first
and then go back to current thread.


HW 1_2 :Implement SYSCALL Log

File "NachOS.log" will be under the following path:
> `NachOS/code/userprog/NachOS.log`

Each time Log(char); called by user program, exception 
handler will output a new line into "NachOS.log" file. 

You can edit user programs by your own inside the path:
> `NachOS/code/test/`

*************************************************************
Release notes for Nachos 4.0.

Nachos 4.0 is somewhat of a re-write of Nachos 3.4.
The main differences are:

1. better software engineering -- every class has its
own selftest routines, 

2. the system is sligthly more portable (it was developed
on an Alpha, and ported to DOS by a student)

3. uses templates for generic data structures

4. reconfiguring for different machines is more 
obvious

5. somewhat different assignments (you can find these
off the Web page, alas they were done in Word, so
they can't be easily edited)

6. the major downside is that it is no longer
in synch with Narten's roadmap (see web page)

My recommendation is that people currently using 3.4
shouldn't bother to use 4.0, unless they need to run
on a 64 bit machine or DOS.  Even those new to Nachos
will find Narten's roadmap probably outweighs
the benefits of using 4.0, except if they need
DOS or Alpha support.
